package template

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func indexTemp(c *gin.Context) {
	c.HTML(http.StatusOK, "index.html", gin.H{"msg": "welcome audible-alarm-go-control!"})
}
