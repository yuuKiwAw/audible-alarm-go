package filecontrol

import (
	"audible-alarm-go/component"
	"audible-alarm-go/global"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
)

var savePath string

func init() {
	savePath = global.Save_path()
}

// 上传文件
func uploadFile(c *gin.Context) {
	file, err := c.FormFile("file")
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"err": err})
		return
	}

	if component.CheckSameFile(savePath, file.Filename) {
		c.JSON(http.StatusOK, gin.H{"code": 1, "msg": "已经存在同名文件，请确认文件名!", "fileName": file.Filename})
	} else {
		if err := c.SaveUploadedFile(file, savePath+file.Filename); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"err": err})
			return
		}
		c.JSON(http.StatusOK, gin.H{"code": 0, "msg": "保存成功。", "fileName": file.Filename})
	}
}

// 获取已经存在的文件列表
func getfileList(c *gin.Context) {
	filelist, count, err := component.GetFileList(savePath)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"err": err})
		return
	}

	c.JSON(http.StatusOK, gin.H{"code": 0, "count": count, "filelist": filelist})
}

// 删除指定文件
func deletefile(c *gin.Context) {
	filename := c.Query("filename")
	err := os.Remove(savePath + filename)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"err": err})
		return
	}

	c.JSON(http.StatusOK, gin.H{"code": 0, "msg": "成功删除 " + filename})
}
