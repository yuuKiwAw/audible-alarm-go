package filecontrol

import "github.com/gin-gonic/gin"

func FilecontrolRouters(e *gin.Engine) {
	filecontrolGroup := e.Group("/filecontrol")
	{
		filecontrolGroup.POST("/upload", uploadFile)
		filecontrolGroup.GET("/filelist", getfileList)
		filecontrolGroup.POST("/delfile", deletefile)
	}
}
