package audiocontrol

import (
	"audible-alarm-go/component"
	"audible-alarm-go/global"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
)

var savePath string

func init() {
	savePath = global.Save_path()
}

// 播放上传文件中指定的文件
func playaudio(c *gin.Context) {
	filename := c.Query("filename")
	musicDir := savePath + filename

	hasFile := component.CheckSameFile(savePath, filename)

	if hasFile {
		copyContext := c.Copy()
		c.JSON(http.StatusOK, gin.H{"code": 0, "msg": "playing ", "filename": filename})
		go func() {
			isplaying, err := component.AudioPlay(musicDir)
			if err != nil {
				c.JSON(http.StatusOK, gin.H{"err": err})
			}
			log.Println(copyContext.Request.URL, "isplaying: ", isplaying)
		}()
	} else {
		c.JSON(http.StatusOK, gin.H{"code": 1, "filename": filename, "msg": "不存在此文件！"})
	}

}
