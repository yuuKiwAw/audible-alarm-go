package configs

import (
	"io/ioutil"
	"log"

	"gopkg.in/yaml.v2"
)

type Config struct {
	WebServer struct {
		Host string `yaml:"host"`
		Port string `yaml:"port"`
	}

	Appcfg struct {
		Uploadfolder string `yaml:"uploadfolder"`
	}
}

func GetConfig() Config {
	config := Config{}
	content, err := ioutil.ReadFile("./configs/cfg.yaml")
	if err != nil {
		log.Fatal(err)
	}

	if yaml.Unmarshal(content, &config) != nil {
		log.Fatal(err)
	}

	return config
}
