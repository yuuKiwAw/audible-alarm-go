package global

import (
	"audible-alarm-go/configs"
	"os"
)

var cfg configs.Config

func init() {
	cfg = configs.GetConfig()
	initUploadFolder()
}

// 检测上传文件夹是否存在，不存在则新建
func initUploadFolder() {
	_, err := os.Stat(Save_path())
	if err != nil {
		if os.IsNotExist(err) {
			os.Mkdir(Save_path(), 0777)
		}
	}
}

// YAML配置文件信息的全局变量
func Cfg() configs.Config {
	return cfg
}

// 上传文件的全局变量
func Save_path() string {
	return "./" + cfg.Appcfg.Uploadfolder + "/"
}
