(this["webpackJsonpaudible-alarm-react"] =
  this["webpackJsonpaudible-alarm-react"] || []).push([
  [0],
  {
    20: function (e, t, l) {},
    27: function (e, t, l) {},
    47: function (e, t, l) {},
    49: function (e, t, l) {
      "use strict";
      l.r(t);
      var a = l(2),
        n = l.n(a),
        c = l(21),
        s = l.n(c),
        i = (l(27), l(11)),
        r = l.n(i),
        o = l(22),
        d = l(6),
        u = l(7),
        h = l(9),
        b = l(8),
        j = l(3),
        p = l.n(j),
        m = (l(20), l(47), l(0)),
        f = (function (e) {
          Object(h.a)(l, e);
          var t = Object(b.a)(l);
          function l() {
            var e;
            Object(d.a)(this, l);
            for (var a = arguments.length, n = new Array(a), c = 0; c < a; c++)
              n[c] = arguments[c];
            return (
              ((e = t.call.apply(t, [this].concat(n))).copyUrl = function () {
                e.props.urlpath && console.log(e.props.urlpath);
              }),
              (e.testApiUrl = function () {
                var t = e.props.urlpath;
                t &&
                  p()({ url: t, method: "POST" })
                    .then(function (e) {
                      console.log(e);
                    })
                    .catch(function (e) {
                      console.log(e), alert("\u64ad\u653e\u51fa\u9519\r" + e);
                    });
              }),
              e
            );
          }
          return (
            Object(u.a)(l, [
              {
                key: "render",
                value: function () {
                  return Object(m.jsx)("div", {
                    children: Object(m.jsxs)("div", {
                      className: "card",
                      style: { marginTop: 540 },
                      children: [
                        Object(m.jsx)("input", {
                          className: "urlText",
                          readOnly: !0,
                          value: this.props.urlpath,
                        }),
                        Object(m.jsx)("button", {
                          className: "textButton",
                          onClick: this.copyUrl,
                          children: "\u590d\u5236",
                        }),
                        Object(m.jsx)("button", {
                          className: "textButton",
                          onClick: this.testApiUrl,
                          children: "\u6d4b\u8bd5",
                        }),
                      ],
                    }),
                  });
                },
              },
            ]),
            l
          );
        })(n.a.Component),
        O = f,
        x = (function (e) {
          Object(h.a)(l, e);
          var t = Object(b.a)(l);
          function l() {
            var e;
            return (
              Object(d.a)(this, l),
              ((e = t.call(this)).getFilesHandle = Object(o.a)(
                r.a.mark(function t() {
                  var l;
                  return r.a.wrap(function (t) {
                    for (;;)
                      switch ((t.prev = t.next)) {
                        case 0:
                          return (
                            (l = []),
                            (t.next = 3),
                            p()({
                              url:
                                e.state.api_urlpath + "/filecontrol/filelist",
                              method: "GET",
                            })
                              .then(function (e) {
                                for (var t = 0; t < e.data.filelist.length; t++)
                                  l.push({
                                    id: t,
                                    filename: e.data.filelist[t],
                                  });
                              })
                              .catch(function (e) {
                                return console.log(e.data);
                              })
                          );
                        case 3:
                          e.setState({ tabledetails: l }),
                            e.clearSelectFileCheckBox();
                        case 5:
                        case "end":
                          return t.stop();
                      }
                  }, t);
                })
              )),
              (e.uploadFileHandle = function (t) {
                if (t.target.files[0]) {
                  var l = new FormData();
                  l.append("file", t.target.files[0]),
                    p()({
                      url: e.state.api_urlpath + "/filecontrol/upload",
                      method: "POST",
                      data: l,
                    })
                      .then(function (t) {
                        console.log(t.data),
                          alert(
                            "code: " + t.data.code + "\rmsg: " + t.data.msg
                          ),
                          0 === t.data.code && e.getFilesHandle();
                      })
                      .catch(function (e) {
                        console.log(e.data);
                      });
                }
              }),
              (e.selectListItemHandle = function (t) {
                var l = t.target.checked,
                  a = t.target.value;
                if (!1 !== l && a in e.state.select_tabledetails === !1)
                  e.state.select_tabledetails.push(a);
                else
                  for (var n = 0; n < e.state.select_tabledetails.length; n++)
                    e.state.select_tabledetails[n] === a &&
                      e.state.select_tabledetails.splice(n, 1);
              }),
              (e.deleteSelectFileHandle = function () {
                if (0 !== e.state.select_tabledetails.length) {
                  for (var t = 0; t < e.state.select_tabledetails.length; t++)
                    console.log(e.state.select_tabledetails[t]),
                      e.deleteFileAPI(e.state.select_tabledetails[t]);
                  alert(
                    "\u5df2\u5220\u9664\u5982\u4e0b!\r" +
                      e.state.select_tabledetails
                  ),
                    e.setState({ select_tabledetails: [] }),
                    e.getFilesHandle();
                } else alert("\u5565\u90fd\u6ca1\u5220\u9664?");
              }),
              (e.deleteFileAPI = function (t) {
                p()({
                  url: e.state.api_urlpath + "/filecontrol/delfile",
                  method: "POST",
                  params: { filename: t },
                })
                  .then(function (e) {
                    return console.log(e.data);
                  })
                  .catch(function (e) {
                    return console.log(e.data);
                  });
              }),
              (e.getUrlpath = function (t) {
                var l = e.state.api_urlpath + "/audiocontrol/play?filename=";
                e.setState({ urlpath: l + t.target.innerHTML }),
                  "hidden" ===
                    document.getElementById("urldisplay").style.visibility &&
                    (document.getElementById("urldisplay").style.visibility =
                      "visible");
              }),
              (e.state = {
                count: 0,
                api_urlpath: "http://localhost:8000",
                urlpath: "",
                tabledetails: [],
                select_tabledetails: [],
              }),
              e
            );
          }
          return (
            Object(u.a)(l, [
              {
                key: "componentDidMount",
                value: function () {
                  this.getFilesHandle(),
                    (document.getElementById("urldisplay").style.visibility =
                      "hidden");
                },
              },
              {
                key: "clearSelectFileCheckBox",
                value: function () {
                  for (
                    var e = document.getElementsByClassName("selectFileCB"),
                      t = 0;
                    t < e.length;
                    t++
                  )
                    e[t].checked = !1;
                },
              },
              {
                key: "render",
                value: function () {
                  var e = this;
                  return Object(m.jsxs)("div", {
                    children: [
                      Object(m.jsxs)("div", {
                        className: "card",
                        children: [
                          Object(m.jsxs)("div", {
                            className: "margin-left-25",
                            children: [
                              Object(m.jsx)("h1", {
                                children: "AudibleControl",
                              }),
                              Object(m.jsx)("div", {
                                className: "margin-bottom-10",
                                children: Object(m.jsxs)("a", {
                                  href: " ",
                                  className: "button-upload",
                                  children: [
                                    "\u9009\u62e9\u6587\u4ef6",
                                    Object(m.jsx)("input", {
                                      type: "file",
                                      name: "upload",
                                      onChange: this.uploadFileHandle,
                                      accept: "audio/mp3",
                                    }),
                                  ],
                                }),
                              }),
                              Object(m.jsxs)("div", {
                                children: [
                                  Object(m.jsx)("button", {
                                    className: "button-normal",
                                    onClick: this.getFilesHandle,
                                    children: "\u83b7\u53d6\u6587\u4ef6",
                                  }),
                                  Object(m.jsx)("button", {
                                    className: "button-normal margin-left-15",
                                    onClick: this.deleteSelectFileHandle,
                                    children: "\u5220\u9664\u6587\u4ef6",
                                  }),
                                ],
                              }),
                            ],
                          }),
                          Object(m.jsx)("div", {
                            className: "margin-25",
                            children: Object(m.jsx)("div", {
                              className: "list-box",
                              children: Object(m.jsxs)("table", {
                                className: "yktable",
                                children: [
                                  Object(m.jsx)("thead", {
                                    children: Object(m.jsxs)("tr", {
                                      children: [
                                        Object(m.jsx)("th", {
                                          className: "selectBox",
                                        }),
                                        Object(m.jsx)("th", {
                                          children: "Index",
                                        }),
                                        Object(m.jsx)("th", {
                                          children: "FileName",
                                        }),
                                      ],
                                    }),
                                  }),
                                  Object(m.jsx)("tbody", {
                                    children: this.state.tabledetails.map(
                                      function (t) {
                                        return Object(m.jsxs)(
                                          "tr",
                                          {
                                            children: [
                                              Object(m.jsx)("td", {
                                                children: Object(m.jsx)(
                                                  "input",
                                                  {
                                                    className: "selectFileCB",
                                                    type: "checkBox",
                                                    onChange:
                                                      e.selectListItemHandle,
                                                    value: t.filename,
                                                  }
                                                ),
                                              }),
                                              Object(m.jsx)("td", {
                                                children: t.id,
                                              }),
                                              Object(m.jsx)("td", {
                                                onClick: e.getUrlpath,
                                                children: t.filename,
                                              }),
                                            ],
                                          },
                                          t.id
                                        );
                                      }
                                    ),
                                  }),
                                ],
                              }),
                            }),
                          }),
                        ],
                      }),
                      Object(m.jsx)("div", {
                        id: "urldisplay",
                        children: Object(m.jsx)(O, {
                          urlpath: this.state.urlpath,
                        }),
                      }),
                    ],
                  });
                },
              },
            ]),
            l
          );
        })(n.a.Component);
      var g = function () {
        return Object(m.jsx)("div", {
          className: "App",
          children: Object(m.jsx)(x, {}),
        });
      };
      s.a.render(
        Object(m.jsx)(n.a.StrictMode, { children: Object(m.jsx)(g, {}) }),
        document.getElementById("root")
      );
    },
  },
  [[49, 1, 2]],
]);
//# sourceMappingURL=main.c6b920e0.chunk.js.map
