package routers

import (
	"audible-alarm-go/middleware/cors"
	"net/http"

	"github.com/gin-gonic/gin"
)

type Option func(*gin.Engine)

var options = []Option{}

func Include(opts ...Option) {
	options = append(options, opts...)
}

func Init() *gin.Engine {
	r := gin.New()
	r.Use(gin.Logger())
	r.Use(cors.Cors())

	/* 静态模板加载 */
	r.LoadHTMLGlob("views/*")
	r.StaticFS("/static", http.Dir("./static"))

	for _, opt := range options {
		opt(r)
	}
	return r
}
