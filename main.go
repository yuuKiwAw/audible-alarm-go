package main

import (
	"audible-alarm-go/app/audiocontrol"
	"audible-alarm-go/app/filecontrol"
	"audible-alarm-go/app/template"
	"audible-alarm-go/global"
	"audible-alarm-go/routers"
	"log"
)

func main() {
	var cfg = global.Cfg()

	routers.Include(template.TemplateRouters, filecontrol.FilecontrolRouters, audiocontrol.AudiocontrolRouters)
	r := routers.Init()

	log.Printf("\n\x1b[%dm[Visit Setting Web in browser] http://localhost:%v"+"/index\n\x1b[0m", 34, cfg.WebServer.Port)
	if err := r.Run(cfg.WebServer.Host + ":" + cfg.WebServer.Port); err != nil {
		log.Fatal(err)
	}
}
