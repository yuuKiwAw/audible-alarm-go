package component

import (
	"io/ioutil"
	"log"
)

// 检测相同的文件
// return bool
func CheckSameFile(path string, filename string) bool {
	rd, err := ioutil.ReadDir(path)
	if err != nil {
		log.Fatalln(err)
	}

	for _, file := range rd {
		if file.Name() == filename {
			return true
		}
	}
	return false
}

// 获取文件夹中的文件
// return 文件名， 文件数
func GetFileList(path string) ([]string, int, error) {
	var count = 0
	var filelist []string
	rd, err := ioutil.ReadDir(path)
	if err != nil {
		log.Fatalln(err)
	}

	for _, file := range rd {
		filelist = append(filelist, file.Name())
		count += 1
	}

	return filelist, count, err
}
