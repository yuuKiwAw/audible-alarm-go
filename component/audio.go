package component

import (
	"log"
	"os"
	"time"

	"github.com/faiface/beep"
	"github.com/faiface/beep/mp3"
	"github.com/faiface/beep/speaker"
)

// 播放音频文件
// return 是否播放中
func AudioPlay(filepath string) (bool, error) {
	isPlaying := true

	f, err := os.Open(filepath)
	if err != nil {
		isPlaying = false
		log.Fatalln(err)
		return isPlaying, err
	}

	streamer, format, err := mp3.Decode(f)

	if err != nil {
		isPlaying = false
		log.Fatalln(err)
		return isPlaying, err
	}

	defer streamer.Close()

	speaker.Init(format.SampleRate, format.SampleRate.N(time.Second/10))

	done := make(chan bool)
	speaker.Play(beep.Seq(streamer, beep.Callback(func() {
		done <- true
	})))

	<-done

	isPlaying = false
	return isPlaying, err
}
