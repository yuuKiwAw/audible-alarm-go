package main

import (
	"audible-alarm-go/component"
	"log"
	"os"
	"os/exec"
)

type PathSet struct {
	build_path  string
	cfg_path    string
	static_path string
	html_path   string
}

/* 初始化项目路径 */
func initPath() PathSet {
	projectPath := "./audible-alarm-server/"
	pathSet := PathSet{
		build_path:  projectPath,
		cfg_path:    projectPath + "configs/",
		static_path: projectPath + "static/",
		html_path:   projectPath + "views/",
	}
	return pathSet
}

func main() {
	log.Println("\naudible-alarm-webservices\nAuthor: yuki\nProject Prepare To Build...\n=====######=====######=====")

	pathSet := initPath()

	deletePackagedFiles(&pathSet) // 0.删除已存在的打包文件
	initConfig(&pathSet)          // 1.初始化配置文件夹
	copyStaticFolder(&pathSet)    // 2.拷贝静态文件夹
	buildProject(&pathSet)        // 3.编译项目
	// zip_project(&pathSet)         // 4.项目打包成压缩文件

	log.Println("Project Build Successfully! Happy Hacking!")
}

/* 编译项目 */
func buildProject(pathSet *PathSet) {
	cmd := exec.Command("go", "build", "-o", pathSet.build_path+"server.exe", "../main.go")
	err := cmd.Run()
	if err != nil {
		log.Println(err)
		return
	}
	log.Println("Compile source successfully!")
}

/* 初始化配置文件夹 */
func initConfig(pathSet *PathSet) {
	createFolder(pathSet.build_path)
	createFolder(pathSet.cfg_path)
	// writeYAMLConfig(pathSet.cfg_path + "cfg.yaml")
	component.CopyDir("../configs", pathSet.cfg_path)
	log.Println("Create config successfully!")
}

// /* 拷贝配置文件信息 */
// func writeYAMLConfig(yaml_path string) {
// 	cfg_value, err := ioutil.ReadFile("../configs/cfg.yaml")
// 	if err != nil {
// 		log.Println(err)
// 		return
// 	}

// 	_, err2 := os.Create(yaml_path)
// 	if err2 != nil {
// 		log.Println(err2)
// 		return
// 	}
// 	ioutil.WriteFile(yaml_path, cfg_value, 0666)
// 	log.Println("Create cfg.yaml successfully!")
// }

/* 拷贝静态文件 */
func copyStaticFolder(pathSet *PathSet) {
	createFolder(pathSet.static_path)
	createFolder(pathSet.html_path)
	component.CopyDir("../static", pathSet.static_path)
	component.CopyDir("../views", pathSet.html_path)
	log.Println("Copy static files successfully!")
}

/* 删除存在的打包文件 */
func deletePackagedFiles(pathSet *PathSet) {
	_, err := os.Stat(pathSet.build_path)
	if err != nil {
	} else {
		os.RemoveAll(pathSet.build_path)
		log.Println("Delete packaged cache")
	}
}

/* 打包成项目压缩文件 */
func zip_project(pathSet *PathSet) {
	component.Zip(pathSet.build_path, "audible-alarm-server.zip")
}

/**
 * 创建新文件夹如果文件不存在的话
 * @param	file_path 需要创建的路径
 */
func createFolder(file_path string) {
	_, err := os.Stat(file_path)
	if err != nil {
		if os.IsNotExist(err) {
			os.Mkdir(file_path, 0777)
		}
	}
}
