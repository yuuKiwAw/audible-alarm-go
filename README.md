<h1 align="center">audible-alarm-go</h1>

<h2 align="center">音频报警播放WebServer-go-gin</h2>

---
---

## TODO

- 接口开发
  - [X] POST 文件上传，包含检测重复文件
  - [X] GET  获取已经存在的文件列表
  - [X] POST 删除指定文件
  - [X] POST 发送指定的连接，播放指定的音频
- 功能新增
  - [X] YAML 配置文件读取
  - [X] 增加程序启动自检
  - [X] 自动打包功能
- 前端页面开发
  - [X] React框架开发控制页面
  - [X] 支持音频文件上传,并返回所有的音频文件
  - [X] 产生相应的音频播放的api url
  - [X] 前端可控制测试音频播放api
  - [X] 打包应用

---
---

## Instructions

- ### Windows

  0. Tips
     - 这三个文件夹为必须依赖，需要和编译完成的二进制文件放在一起
       - configs
       - static
       - views
  1. 你可以直接在根目录编译运行
     - go build main.go
     - .\main.exe
  2. 或者可以运行自动打包程序（自动打包依赖文件）
     - cd build
     - go run build.go
       - 同时会生成zip压缩包 audible-alarm-server.zip
       - cd audible-alarm-server
       - .\server.exe
  3. 你或许需要修改静态文件下的API URL让其他机器能够访问到此服务器的API
     - main.c6b920e0.chunk.js
     - main.c6b920e0.chunk.js.map
     - 修改里面的api_urlpath后的host,修改成当前server的ip