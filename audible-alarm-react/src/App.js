import AudibleControl from "./component/AudibleControl/index.js";

function App() {
  return (
    <div className="App">
      <AudibleControl />
    </div>
  );
}

export default App;
