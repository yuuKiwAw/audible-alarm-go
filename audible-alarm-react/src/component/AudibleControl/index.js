import React from "react";
import axios from "axios";
import Urldisplay from "./Urldisplay";
import "./index.css";

class AudibleControl extends React.Component {
  constructor() {
    super();
    this.state = {
      count: 0,
      api_urlpath: "http://localhost:8000",
      urlpath: "",
      tabledetails: [],
      select_tabledetails: [],
    };
  }

  componentDidMount() {
    this.getFilesHandle();
    document.getElementById("urldisplay").style.visibility="hidden";
  }

  /* 获取文件列表 */
  getFilesHandle = async () => {
    let filelist = [];

    await axios({
      url: this.state.api_urlpath + "/filecontrol/filelist",
      method: "GET",
    })
      .then(res => {
        for (var i = 0; i < res.data.filelist.length; i++) {
          filelist.push({id: i, filename: res.data.filelist[i]});
        }
      })
      .catch(err => console.log(err.data));

    this.setState({
      tabledetails: filelist,
    });
    this.clearSelectFileCheckBox();
  };

  /* 上传单个文件 */
  uploadFileHandle = e => {
    if (!e.target.files[0]) {
      return;
    }
    var data = new FormData();
    data.append("file", e.target.files[0]);

    axios({
      url: this.state.api_urlpath + "/filecontrol/upload",
      method: "POST",
      data: data,
    })
      .then(res => {
        console.log(res.data);
        alert("code: " + res.data.code + "\rmsg: " + res.data.msg);
        if (res.data.code === 0) {
          this.getFilesHandle();
        }
      })
      .catch(err => {
        console.log(err.data);
      });
  };

  /* 获取选中的文件名称保存到列表 */
  selectListItemHandle = e => {
    let checkbox_value = e.target.checked;
    let select_filename = e.target.value;

    if (checkbox_value !== false && select_filename in this.state.select_tabledetails === false) {
      this.state.select_tabledetails.push(select_filename);
    } else {
      for (let i = 0; i < this.state.select_tabledetails.length; i++) {
        if (this.state.select_tabledetails[i] === select_filename) {
          this.state.select_tabledetails.splice(i, 1);
        }
      }
    }
  };

  /* 删除选中列表中的文件 */
  deleteSelectFileHandle = () => {
    if (this.state.select_tabledetails.length !== 0) {
      for (let i = 0; i < this.state.select_tabledetails.length; i++) {
        console.log(this.state.select_tabledetails[i]);
        this.deleteFileAPI(this.state.select_tabledetails[i]);
      }
      alert("已删除如下!\r" + this.state.select_tabledetails);
      this.setState({select_tabledetails: []});
      this.getFilesHandle();
    } else {
      alert("啥都没删除?");
      return;
    }
  };

  /* 清空checkbox列表 */
  clearSelectFileCheckBox() {
    let checkbox_list = document.getElementsByClassName("selectFileCB");
    for (let i = 0; i < checkbox_list.length; i++) {
      checkbox_list[i].checked = false;
    }
  }

  /* 后端删除接口 */
  deleteFileAPI = filename_str => {
    axios({
      url: this.state.api_urlpath + "/filecontrol/delfile",
      method: "POST",
      params: {
        filename: filename_str,
      },
    })
      .then(res => console.log(res.data))
      .catch(err => console.log(err.data));
  };

  getUrlpath = e => {
    let base_url = this.state.api_urlpath + "/audiocontrol/play?filename=";
    this.setState({urlpath: base_url + e.target.innerHTML});

    if (document.getElementById("urldisplay").style.visibility === "hidden") {
      document.getElementById("urldisplay").style.visibility = "visible";
    }
  };

  render() {
    return (
      <div>
        <div className="card">
          <div className="margin-left-25">
            <h1>AudibleControl</h1>
            <div className="margin-bottom-10">
              <a href=" " className="button-upload">
                选择文件
                <input type={"file"} name="upload" onChange={this.uploadFileHandle} accept="audio/mp3"></input>
              </a>
            </div>
            <div>
              <button className="button-normal" onClick={this.getFilesHandle}>
                获取文件
              </button>
              <button className="button-normal margin-left-15" onClick={this.deleteSelectFileHandle}>
                删除文件
              </button>
            </div>
          </div>

          <div className="margin-25">
            <div className="list-box">
              <table className="yktable">
                <thead>
                  <tr>
                    <th className="selectBox"></th>
                    <th>Index</th>
                    <th>FileName</th>
                  </tr>
                </thead>

                <tbody>
                  {this.state.tabledetails.map(item => (
                    <tr key={item.id}>
                      <td>
                        <input
                          className="selectFileCB"
                          type="checkBox"
                          onChange={this.selectListItemHandle}
                          value={item.filename}
                        />
                      </td>
                      <td>{item.id}</td>
                      <td onClick={this.getUrlpath}>{item.filename}</td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>

        <div id="urldisplay">
          <Urldisplay urlpath={this.state.urlpath} />
        </div>
      </div>
    );
  }
}

export default AudibleControl;
