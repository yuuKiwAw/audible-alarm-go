import axios from "axios";
import React from "react";
import "../index.css";
import "./index.css";

class Urldisplay extends React.Component {
  copyUrl = () => {
    let url = this.props.urlpath;
    if (!url) return;
    console.log(this.props.urlpath);
  };

  testApiUrl = () => {
    let url = this.props.urlpath;
    if (!url) return;
    axios({
      url: url,
      method: "POST",
    })
      .then(res => {
        console.log(res);
      })
      .catch(err => {
        console.log(err);
        alert("播放出错\r" + err);
      });
  };

  render() {
    return (
      <div>
        <div className="card" style={{marginTop: 540}}>
          <input className="urlText" readOnly value={this.props.urlpath}></input>
          <button className="textButton" onClick={this.copyUrl}>
            复制
          </button>
          <button className="textButton" onClick={this.testApiUrl}>
            测试
          </button>
        </div>
      </div>
    );
  }
}

export default Urldisplay;
